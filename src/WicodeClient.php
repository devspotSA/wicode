<?php

namespace WorkshopOrange\Wicode;

define('RET_WICODE_SUCCESS',-1);

class WicodeClient {
    
    const CALL_GET = 1;
    const CALL_POST = 2;
    
    private $apiId = null;
    private $apiPassword = null;
    private $baseUrl = null;
    private $voucherRequestPath = null;
    private $test_campaign_ids = [];
    
    
    /**
     * WicodeClient constructor.
     * @param null $apiId
     * @param null $apiPassword
     * @param null $baseUrl
     * @param null $voucherRequestPath
     * @param array $test_campaign_ids
     */
    public function __construct($apiId = null,
      $apiPassword = null,
      $baseUrl = null,
      $voucherRequestPath = null,
      $test_campaign_ids = [])
    {
        $this->apiId = $apiId;
        $this->apiPassword = $apiPassword;
        $this->baseUrl = $baseUrl;
        $this->voucherRequestPath = $voucherRequestPath;
        $this->test_campaign_ids = $test_campaign_ids;
    }
    
    /**
     * Dump the config for the active partner
     * @return mixed
     */
    public function dump_config() {
        return [
          'apiId' => $this->apiId,
          'apiPassword' => $this->apiPassword,
          'baseUrl' => $this->baseUrl,
          'voucherRequestPath' => $this->voucherRequestPath,
          'test_campaign_ids' => $this->test_campaign_ids
        ];
    }
    
    /**
     * Run a test on the integration
     * @return mixed
     * @throws \Exception
     */
    public function testRequestVoucher() {
        $ret = [
          'status' => FALSE,
          'data' => []
        ];
        
        if(!is_array($this->test_campaign_ids) || count($this->test_campaign_ids) <= 0) {
            throw new \Exception("You can't test without test_campaign_ids");
        }
        
        $ret['data']['number_of_test_campaigns'] = count($this->test_campaign_ids);
        
        $campaign = $this->test_campaign_ids[rand(0, count($this->test_campaign_ids) - 1)];
        $ret['data']['campaign'] = $campaign;
        
        $user_ref = uniqid();
        $ret['data']['user_ref'] = $user_ref;
        
        $server_response = $this->requestVoucher($campaign, $user_ref);
        $ret['data']['server_response'] = $server_response;
        
        if(is_object($server_response) && $server_response->responseCode == RET_WICODE_SUCCESS) {
            $ret['status'] = TRUE;
        }
        
        return $ret;
    }
    
    /**
     * @param $campaignId
     * @param $userRef
     * @return mixed
     */
    public function requestVoucher($campaignId, $userRef)
    {
        $response = $this->doCall($this->baseUrl . $this->voucherRequestPath, compact('campaignId','userRef'));
        
        return json_decode($response);
    }
    
    /**
     * Wrapper for cURL calls
     * @param String $url
     * @param Array data - keyvalue of data being passed via GET/POST
     * @param int $method
     * @return String response
     * @throws \Exception
     */
    private function doCall($url, $data, $method = self::CALL_POST) {
        
        $dataString = json_encode($data);
        
        $ch = curl_init();
        
        switch($method) {
            case(self::CALL_POST):
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                  'apiId:'.$this->apiId,
                  'apiPassword:'.$this->apiPassword,
                  'Content-Type:'.'application/json'
                ));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                break;
            case(self::CALL_GET):
                curl_setopt($ch, CURLOPT_URL, $url.'?'.$dataString);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                break;
            default:
                throw new \Exception("Unknown CURL Method invoked");
                break;
        };
        
        $responseData = curl_exec($ch);
        
        if(curl_errno($ch)) {
            throw new \Exception("Communications error:". curl_error($ch));
        }
        
        return $responseData;
    }
}
