<?php namespace WorkshopOrange\Wicode;

use Dotenv;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use WorkshopOrange\Wicode\WicodeClient;

class TestWicodeCommand extends Command
{
    const SERVER_NOT_FOUND = 240;
    const CONFIG_FILE_DEFAULT = "wicode.yaml";
    
    protected $config = [];
    
    protected function configure()
    {
        parent::configure();
        
        $this
          ->setName('test:wicode')
          ->setDescription('Test the WiCode Integration')
          ->setHelp("Test if the wicode integration is working");
        
        $this->addOption('config','c',
          InputOption::VALUE_REQUIRED,
          'Specify a config file path',
          getcwd() . "/" . self::CONFIG_FILE_DEFAULT);
        
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configFilePath = $input->getOption('config');
        $output->writeln("Config: " . $configFilePath);
        
        if(file_exists($configFilePath)) {
            $this->config = Yaml::parse(file_get_contents($configFilePath));
        } else {
            throw new \Exception("Config not found.");
        }
        
        $table = new Table($output);
        $tableRows = [];
        
        foreach($this->config as $k => $v) {
            if(is_array($v)) {
                $tableRows[] = [$k, implode(", ", $v)];
            } else {
                $tableRows[] = [$k, $v];
            }
        }
        
        $table
          ->setHeaders(array('Key', 'Value'))
          ->setRows($tableRows);
        
        $table->render();
        
        $client = new WicodeClient(
          $this->config['apiId'],
          $this->config['apiPassword'],
          $this->config['baseUrl'],
          $this->config['voucherRequestPath'],
          $this->config['test_campaign_ids']
        );
        
        $test = $client->testRequestVoucher();
        
        if(!$test['status']) {
            $output->writeln("The test failed. Data follows: ");
            $output->writeln(print_r($test['data'], TRUE));
        } else {
            $output->writeln("The test passed! Data follows: ");
            $output->writeln(print_r($test['data'], TRUE));
        }
    }
}
